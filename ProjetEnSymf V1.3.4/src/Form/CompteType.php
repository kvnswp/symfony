<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\RangeType;

class CompteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('firstName')
            ->add('email')
            ->add('cpu', RangeType::class, [
                'attr' => [
                'min' => 2,
                'max' => 32,
                'step'=> 6
                ]
                ])
            ->add('ram', RangeType::class, [
                'attr' => [
                'min' => 4,
                'max' => 128,
                'step'=> 7
                ]
                ])
            ->add('gb', RangeType::class, [
                'attr' => [
                'min' => 60,
                'max' => 250,
                'step'=> 19
                ]
                ]);
            
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
