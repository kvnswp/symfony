<?php
 
namespace App\Controller; 

use App\Entity\User;
use App\Entity\Service;
use App\Form\CompteType;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextaeraType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ServicePcController extends AbstractController
{

     /**
     * @Route("/home", name="home")
     */
    public function home()
    { 
        /* recuperer les données */
        $repo= $this->getDoctrine()->getRepository(Service::class);
        $services= $repo -> findAll();

        return $this->render('service_pc/home.html.twig',[
            'services' => $services
        ]);
    }

     /**
     * @Route("/profil", name="profil")
     */
    public function profil()
    {
        return $this->render('service_pc/profil.html.twig');
    }
     /**
     * @Route("/connexion", name="connexion")
     */
    public function connexion()
    {
        return $this->render('service_pc/connexion.html.twig');
    }
     /**
     * @Route("/deconnexion", name="deconnexion")
     */
    public function deconnexion()
    {
        return $this->render('service_pc/deconnexion.html.twig');
    }
    /**
     * @Route("/service/{id}", name="service")
     */
    public function create(Request $request, ObjectManager $manager, Service $service/*, UserPasswordEncoderInterface $encoder*/)
    {   
        $user = new User();

        $form=$this->createForm(CompteType::class, $user);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            /*$hash = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($hash);*/
            $manager->persist($user);
            $manager->flush();

            return $this->redirectToRoute('connexion');
        }

        return $this->render('service_pc/subscribe.html.twig', [
            'formUser' => $form->createView(),
            'service' => $service
        ]);
    }
}
