<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextaeraType;
use App\Form\CompteType;
use App\Entity\Service;
use App\Entity\User;

class ServicePcController extends AbstractController
{

     /**
     * @Route("/home", name="home")
     */
    public function home()
    {
        /* recuperer les données */
        $repo= $this->getDoctrine()->getRepository(Service::class);
        $services= $repo -> findAll();

        return $this->render('service_pc/home.html.twig',[
            'services' => $services
        ]);
    }

     /**
     * @Route("/profil", name="profil")
     */
    public function profil()
    {
        return $this->render('service_pc/profil.html.twig');
    }
     /**
     * @Route("/connexion", name="connxeion")
     */
    public function connexion()
    {
        return $this->render('service_pc/connexion.html.twig');
    }
     /**
     * @Route("/deconnexion", name="deconnexion")
     */
    public function deconnexion()
    {
        return $this->render('service_pc/deconnexion.html.twig');
    }
    /**
     * @Route("/service", name="service")
     */
    public function create(Request $request, ObjectManager $manager)
    {   
        $user = new User();

        /* Créer form manuellement */
       /* $form = $this -> createFormBuilder($user)
                      -> add('name')
                      -> add('firstName')
                      -> add('email')
                      -> getForm();*/

        /* Créer form automatiquement à partir de cmd */ 
        $form=$this->createForm(CompteType::class, $user);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $manager->persist($user);
            $manager->flush();

            return $this->redirectToRoute('home');
        }

        return $this->render('service_pc/subscribe.html.twig', [
            'formUser' => $form->createView()
        ]);
    }
}
